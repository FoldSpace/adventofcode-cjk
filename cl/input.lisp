(in-package #:aoc)

(defun to-array (lst &optional shape)
  (let ((s (if shape
               shape
               (list (length lst)))))
    (make-array s :initial-contents lst)))

(defun cookie-file ()
  (mapcar (lambda (x) (str:split #\: x))
          (str:lines
           (str:from-file "../../../aoc-cookies.txt"))))

(defun get-cookies ()
  (make-instance
   'drakma:cookie-jar
   :cookies (maplist
             (lambda (x)
               (make-instance
                'drakma:cookie
                :name (caar x)
                :value (cadar x)
                :domain ".adventofcode.com"))
            (cookie-file))))

(defun request-input (year day file)
  (let ((url "https://adventofcode.com/~d/day/~d/input"))
    (with-open-file (stream file :direction :output)
                    (format stream
                            (drakma:http-request
                             (format 'nil url year day)
                             :method :get
                             :cookie-jar (get-cookies))))))

(defun input-for-day (year day)
  (let* ((folder (asdf:system-relative-pathname
                  (intern (package-name *package*))
                  (format NIL "../~d/" year)))
         (file (format nil "~a~d.txt" folder day)))
    (flet ((get-file (f) (str:lines (str:from-file f))))
          (progn
           (ensure-directories-exist folder)
           (if (probe-file file)
               (get-file file)
               (progn (request-input year day file)
                      (get-file file)))))))
