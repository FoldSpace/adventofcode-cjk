 ⎕IO←0
 day1←{
     CD ← (1 0)(0 1)(¯1 0)(0 ¯1)             ⍝ [c]ardinal [d]irections
     p  ← (⊃,(⍎1↓⊢))⍤1                ⍝ [p]arse (char, int)
     in ← {↓⍉p↑','(≠⊆⊢)⍵~' '}         ⍝ [in]put split on ','
     o  ← {CD[4|+\1 ¯1⌷⍨⊂'L'=⍵]}    ⍝ [o]rientation sequence
     a  ← { +/ ⊃| ⍺⍺ ⍵/o ⍺}           ⍝ distance taking ⍵ steps in orientation ⍺
     p1 ← {+⌿⍣2⊢↑ ⊃,/⍵}a              ⍝ # steps from cell 0 0 to  final coordinates
     ⍝ p2←{({⊂⍸~≠⍵}⌷⊢)+\⍵}a           ⍝ need to test in APRIL
     p2 ← {((⊂⍸)∘(⍳∘≢ ≠ ⍳⍨) ⌷ ⊢)+\⍵}a ⍝ # steps to first non-unique cell
     (p1/, p2/)in ⊃⍵
 }

 day2←{
     CD ← (+,-)(1 0)(0 1)
     in ← {CD⌷⍨⊂'DRUL'⍳⍵}¨

     ⍝ [r]ecursively accum the steps(⍺⍺/) for each row
     r←{0=≢⍵:1↓⍺◊(⍺,⍺⍺/⌽(¯1↑⍺),⊃⍵)∇1↓⍵}

     p1←{pad  ← 3 3⍴1+⍳9                 ⍝ number pad for problem 1
         step ← {0⌈2⌊⍺+⍵}                ⍝ take [step] staying inbounds
         pin  ← {(,⊂0 0)(step r) ⍵}      ⍝ find pin sequence for numpad
         ⊂pad[pin ⍵]}                    ⍝ valid numpad sequence

     p2←{mask ← 1(⊖⍪⍪)1(⌽,,)2 2↑1        ⍝ bit mask for the numpad
         inds ← ⍸mask                    ⍝ indices into the numpad
         pad  ← ('ABCD',⍨1+⍳9)@inds⊢mask ⍝ numberpad
         step ← {(⊂x←⍺+⍵)∊inds : x ⋄ ⍵}  ⍝ calculate next inbounds step
         pin  ← {(⊂2 0)(step r) ⍵}       ⍝ find pin sequence for numberpad
         ⊂pad[pin ⍵]}                    ⍝ valid numpad sequence

     (p1, p2) in ⍵

     ⍝ day2←{ credit to rak1507 for this clever rewrite of my solution
     ⍝     p1 ← 0⍪3 3⍴1+⍳9                          ⍝ pad the number pads with 0's
     ⍝     p2 ← 0,('ABCD',⍨1+⍳9)@⊢3>∘.+⍨|2-⍳5       ⍝ so that they can use the same start index
     ⍝     r  ← {(⊂x←⍵+9 11○0J1*'DRUL'⍳⍺)∊⍺⍺:x ⋄ ⍵} ⍝ "complex numbers are common for grid coord problems"
     ⍝     p1 p2 ∘.{⍺ ⊃⍨ ((⍸⍺≠0)r)/(⌽⍵),⊂2 1} ⍵     ⍝ find first index where ≠0
     ⍝ }
 }

 day3←{
     in ← ⍎⍤1∘↑                         ⍝ parse the entire matrix as ints
     p1 ← +/{((+/2↑⊢) > ⊃∘⌽) ⍵[⍋⍵]}⍤1   ⍝ sum of the 2 smallest > the largest
     p2 ← p1∘{((3÷⍨≢⍵),3)⍴⍵[⍋(≢⍵)⍴⍳3]}, ⍝ p1, grouping chunks of 3 numbers columnwise
     (p1, p2) in ⍵
 }

 day4←{⎕←⍵}
 day5←{⎕←⍵}
 day6←{⎕←⍵}
 day7←{⎕←⍵}
 day8←{⎕←⍵}

 day9←{
     O C ← '()'                      ⍝ [o]pen and [c]lose parens
     e←{0 = ≢⍵: 0                    ⍝ [e]xpand until length 0 input
        O ≠ ⊃⍵: 1 + ∇ 1↓⍵            ⍝ add 1 when first is alphanumeric
        i   ← 1+⍵⍳C                  ⍝ end of the first (enclosure)
        n r ← ⍎⊃,/(' '@{'x'=⍵})i↑⍵   ⍝ [n]umber of chars, [r]epetitions
        s   ← n↑i↓⍵                  ⍝ [s]egment starting after (enclosure)
        (r × ⍺⍺ s) + ∇(i+n)↓⍵}       ⍝ count of segment + expand rest of input

     p1 ← ≢e                         ⍝ count segment
     p2 ← {p2 e ⍵}e                  ⍝ recursively expand segment
     (p1, p2) ⍵
 }