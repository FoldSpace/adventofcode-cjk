(in-package :aoc-16)

(defvar *year* 2016) ; defvar is dynamic
(defvar *file-name* "./16/year.apl") ; defvar is dynamic

(april
 (with
  (:store-fun
   (sh (lambda (x)
         (to-array
          (uiop:run-program
           (coerce x 'string)
           :output :string))))
   (lc  (lambda (x)   (string-downcase x)))
   (and (lambda (x y) (logand x y)))
   (or  (lambda (x y) (logior x y)))
   (lshift (lambda (x) (ash x  1)))
   (rshift (lambda (x) (ash x -1)))
   (fn (lambda (x) (+ 1 x)))))
 "")
