(in-package #:aoc)

(defvar *year* "15")

(defun set-year (year)
  (let ((year-string (write-to-string year)))
    (setf *year* (subseq year-string (- (length year-string) 2)
                         (length year-string)))
    (format t "~%~%∘○∘ Loading solutions from year 20~a... ∘○∘~%~%" *year*)
    (asdf:load-system (intern (format nil "AOC-~a" *year*)))))

(defmacro day (day &rest parser)
  ;; (eval '(april-load (asdf:system-relative-pathname
  ;;                     (intern (package-name *package*) "KEYWORD")
  ;;                     *file-name*)))
  (let* ((sym 'input)
         (value (if (null parser)
                    sym
                    `(progn ,@parser))))
    `(let* ((,sym (input-for-day ,(format nil "20~a" *year*) ,day))
            (inp ,value))
       (april-c ,(format nil "day~d" day)
                (to-array inp)))))


(defmacro defapl (name args &rest body)
  (let* ((splits (str:split "." (cadr name)))
         (ns     (first (butlast splits)))
         (fname  (first (last splits))))
    `(defun ,(car name) ,args
       (april-c ,(if ns `(with (:space ,(intern (string-upcase ns)))))
                ,fname
                (to-array ,@body)))))
