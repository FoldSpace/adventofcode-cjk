(defpackage #:aoc
  (:shadowing-import-from #:str #:EmptyP)
  (:use #:cl #:april)
  (:export #:to-array))
