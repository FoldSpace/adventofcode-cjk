 ⎕IO ← 0
 day1←{
    in ← {1-⍨') ('⍳⍵}                    ⍝ [in]put
    p1 ← +/                              ⍝ sum
    p2 ← {⊃⍸¯1=+\⍵}                      ⍝ first non-positive step
    (p1,p2) in ⍵                         ⍝ all results
 }

 day2←{
    in ← ⍎¨∘('x'∘≠⊆⊢)¨                   ⍝ '10x20x30' ←→ 10 20 30
    p1 ← +/(+/∘(⌊/,2∘×)(1∘⌽×⊢))¨         ⍝ sum min + 2 × adjacent prods
    p2 ← +/(×/++/∘(2×(⊂2↑⍋)⌷⊢))¨         ⍝ sum of prod + 2 × the 2 smallest
    (p1,p2) in ⍵                         ⍝ sum each result
 }

 day3←{
    dt ← 'v><^'                          ⍝ [d]irection [t]ravelled
    cp ← ,∘.,⍨1 ¯1                       ⍝ [c]ardinal  [p]airs of diagonal grid
    lm ← {≢∪ ⍺⍺ ⍵}                       ⍝ [l]ength of unique [m]oves
    in ← {0⍪↑cp⌷⍨⊂dt⍳⍵}                  ⍝ cardinal moves travelled table
    p1 ← +⍀ lm                           ⍝ sum expand all cp
    p2 ← {,↓+⍀⍤2⊢(2|⍳≢⍵)⊢⌸⍵} lm          ⍝ sum expand alternating groups
    (p1,p2) in ⍵
 }

 day4←{
     md ← {'echo -n ''',⍵,''' | md5sum'} ⍝ shell [md]5sum
     ex ← {sh md ⍵}                      ⍝ [ex]ecute md5sum for string ⍵
     do ← {⍺⍺ ex ⍺,⍕⍵ : ⍵ ◊ ⍺ ∇ ⍵+1}     ⍝ recurse until match is found
     fn ← {⍺∧.=(≢⍺)⍴⍵}                   ⍝ ⍺ matches first n from ⍵
     p1 ← (5⍴'0')∘fn do                  ⍝ match 5 leading 0's in [md]
     p2 ← (6⍴'0')∘fn do                  ⍝ match 6 leading 0's in [md]
     ⍝ ⍵ (⊢,p2) ⍵ p1 1                   ⍝ works in theory, but very slow
 }

 day5←{
     p1←{nt ← 'ab' 'cd' 'pq' 'xy'        ⍝ [n]augh[t]y
         nn ← nt∘((~∨/)(∨/⍷)¨)⊂          ⍝ [n]ot [n]aughty ←→ is nice
         ma ← ∨/2=/⊢                     ⍝ [m]atch [a]djacent 'xx'
         vc ← 3≤1⊥∊∘'aeiou'              ⍝ [v]owel [c]ount is at least 3
         +/(nn ∧ ma ∧ vc)¨⍵ }            ⍝ how many are all true?

     p2←{twilap ← (≢≠≢∘∪)∧(∧/2≢/⊢)       ⍝ [twi]ce with no over[lap]
         pe     ← ∨/∘((⌽≡⊢)¨)3,/⊢        ⍝ len 3 [p]alindrome [e]xists 'xyx'
         ap     ← ∨/∘(twilap)2,/⊢        ⍝ [a]djacent [p]airs 'xyxy' or 'aacaa'
         +/(pe∧ap)¨⍵ }                   ⍝ how many are all true?

     (p1,p2) ⍵
 }

day6←{
     in←{sf←{1↓¯5↑' '(≠⊆⊢)⍵}             ⍝ [s]plit  [f]ields
         a s _ e←sf ⍵                    ⍝ [a]ction [s]tart [e]nd
         (f t)←⍎¨∘(','∘≠⊆⊢)¨s e          ⍝ [f]rom [t]il denotes a square
         (⊃⌽a) f t}¨

     ra←{0=≢⍵:1⊥⍺
         a f t←⊃⍵                        ⍝ [a]ction [f]rom [t]il
         n←⍺⍺ ◊ e←⍵⍵                     ⍝ [n]←on/off ◊ [e]←toggle
         g←{'e'=a:e ⍵◊n a}               ⍝ toggle or on off
         r←{⍺∘+¨⍳1+⍵-⍺}                  ⍝ [r]ange of indices in ⍺
         x←f r t                         ⍝ fart
         ((0⌈g@x)⊢⍺) ∇ 1↓⍵ }             ⍝ perform g over r in a
    (999 999⍴0) ({'n'=⍵} ra ~)in ⍵
 }


 day7←{
     in←↑{(⊂0 1 2 4)⌷¯5↑' '∘(≠⊆⊢) lc ⍵}¨
     {⍵[;3]}in ⍵
     ⍝ rshift 4
     ⊢⊣
 }