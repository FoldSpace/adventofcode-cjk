;;;; aoc-15.asd

(asdf:defsystem #:aoc-15
  :description "Solutions to Advent of Code 2015"
  :author "Your Name <your.name@example.com>"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :depends-on ("aoc")
  :components ((:file "package")
               (:file "year")))
