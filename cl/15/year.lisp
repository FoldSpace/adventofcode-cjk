(in-package :aoc-15)

(defun size (n)
  "floor(log(n)/log(2)) + 1"
  (+ 1 (floor (log 2 n))))


(april
 (with
  (:store-fun
   (sh (lambda (x)
         (to-array
          (uiop:run-program
           (coerce x 'string)
           :output :string))))
   (lc  (lambda (x)   (string-downcase x)))
   (and (lambda (x y) (logand x y)))
   (or  (lambda (x y) (logior x y)))
   (lshift (lambda (x) (ash x  1)))
   (rshift (lambda (x) (ash x -1)))
   (fn (lambda (x) (+ 1 x)))))
 "")


(april-load (asdf:system-relative-pathname
              (intern (package-name *package*) "KEYWORD")
              "./year.apl"))
