(in-package #:aoc)

(defun json->ns (json-str)
  (let ((yason:*parse-object-key-fn*
         (lambda (item) (intern item "KEYWORD")))
        (yason:*parse-object-as* :plist)
        (yason:*parse-json-arrays-as-vectors* t))
    (yason:parse json-str)))

(defmacro make-request (name cb &rest body)
  `(april-c
    (with (:state :in
            ((,name
              (json->ns
               (flexi-streams:octets-to-string
                (drakma:http-request
                 ,@body)))))))
     ,cb))

