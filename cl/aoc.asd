;;;; aoc.asd

(asdf:defsystem #:aoc
  :description "library for advent of code solutions"
  :author "Cpt Kirk"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :depends-on ("april" "str" "uiop" "drakma" "yason")
  :components ((:file "package")
               (:file "days")
               (:file "json")
               (:file "input")))
